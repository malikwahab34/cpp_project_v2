#include <iostream>
#include "Containers.h"
#include <gtest/gtest.h>
using namespace std;


TEST(ContainersTest, List) { 


    Containers list_container;
    list<int> temporary_list;
    temporary_list.push_back(4);
    temporary_list.push_back(12);
    temporary_list.push_back(7);
    list_container.set(temporary_list);
    ASSERT_EQ(209, list_container.get_sumOfSquares());
    ASSERT_EQ(3, list_container.get_size());
    ASSERT_EQ(4, list_container.get_minimum());
    ASSERT_EQ(12, list_container.get_maximum());

}

TEST(ContainersTest, Vector) { 


    Containers vector_container;
    vector<int> temporary_list;
    temporary_list.push_back(7);
    temporary_list.push_back(8);
    temporary_list.push_back(2);
    vector_container.set(temporary_list);
    ASSERT_EQ(117, vector_container.get_sumOfSquares());
    ASSERT_EQ(3, vector_container.get_size());
    ASSERT_EQ(2, vector_container.get_minimum());
    ASSERT_EQ(8, vector_container.get_maximum());

}



int main(int argc, char **argv) {
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}