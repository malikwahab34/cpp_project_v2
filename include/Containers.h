#ifndef CONTAINERS_H
#define CONTAINERS_H

#include<iostream>
#include <vector>
#include <list>
#include <iterator>
using namespace std;

// Structure for the stats
struct class_struct
    {
        int sum_of_squares;
        int minimum;
        int maximum;
        int size;
    };

class Containers
{
    private:
       class_struct stats;

    public:
        void print();
        Containers();
        ~Containers();
        int get_sumOfSquares();
        int get_size();
        int get_minimum();
        int get_maximum();
        void set(vector<int>);
        void set(list<int>);
        vector<int> Squared (vector<int> input);
        list<int> Squared(list<int> input);

};

#endif