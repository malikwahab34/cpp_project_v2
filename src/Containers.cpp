#include "Containers.h"
#include <iostream>
#include <string>
#include <cmath>
#include <list>
#include <iterator>
#include <bits/stdc++.h> 
using namespace std;

void Containers::print()
{
    cout << "Hello World!" << endl;
}

Containers::Containers()
{
    cout << "Constructor called" << endl;
    stats.sum_of_squares = 0;
    stats.maximum = 0;
    stats.minimum = 0;
    stats.size = 0;
}

Containers::~Containers()
{
    cout << "Distructor called" << endl;
}

vector<int> Containers::Squared(vector<int> input)
{
    vector<int> output;
    for(auto i = input.cbegin(); i != input.cend(); i++ )
    {
        output.push_back(pow(*i,2));
    }
    return output;
}

list<int> Containers::Squared(list<int> input)
{
    list<int> output;
    list <int> :: iterator it; 
    for (it = input.begin(); it != input.end(); it++)
    {
        output.push_back(pow(*it,2));
    }
    return output;
}

void Containers::set(vector<int> input)
{
    
    for (auto i = input.cbegin(); i != input.cend(); i++)
    {
        stats.sum_of_squares = stats.sum_of_squares + pow(*i, 2);
    }
    
   stats.size = input.size();
   sort(input.begin(), input.end());
   stats.minimum = input[0];
   stats.maximum = input[stats.size - 1];

}

void Containers::set(list<int> input)
{
    for (auto i = input.begin(); i != input.end(); i++)
    {
        stats.sum_of_squares = stats.sum_of_squares + pow(*i, 2);
    }
    
    stats.size = input.size();
    input.sort();
    stats.minimum = input.front();
    stats.maximum = input.back();
}

int Containers::get_sumOfSquares()
{
    return stats.sum_of_squares;
}

int Containers::get_maximum()
{
    return stats.maximum;
}

int Containers::get_minimum()
{
    return stats.minimum;
}

int Containers::get_size()
{
    return stats.size;
}