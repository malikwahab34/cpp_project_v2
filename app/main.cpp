#include <iostream>
#include "Containers.h"
using namespace std;

int main()
{
    Containers list_container;
    Containers vector_container;
    // Creating an example list to check the functionality on a list.
    cout << "Output for list " << endl;
    list<int> temporary_list;
    temporary_list.push_back(1);
    temporary_list.push_back(2);
    temporary_list.push_back(4);
    temporary_list.push_back(10);
    temporary_list.push_back(15);

    list_container.set(temporary_list);
    cout << "Maximum is " << list_container.get_maximum() << endl;
    cout << "Minimum is " << list_container.get_minimum() << endl;
    cout << "Size is " << list_container.get_size() << endl;
    cout << "Sum of squares is " << list_container.get_sumOfSquares() << endl;

    // Creating an example vector to check the functionality on a vector.
    cout << "Output for vector" << endl;
    vector<int> temporary_vector;
    temporary_vector.push_back(1);
    temporary_vector.push_back(2);
    temporary_vector.push_back(4);
    temporary_vector.push_back(10);
    temporary_vector.push_back(15);

    vector_container.set(temporary_vector);
    cout << "Maximum is " << vector_container.get_maximum() << endl;
    cout << "Minimum is " << vector_container.get_minimum() << endl;
    cout << "Size is " << vector_container.get_size() << endl;
    cout << "Sum of squares is " << vector_container.get_sumOfSquares() << endl;
    
}   