# C++ Project
## This program takes input a list or a vector and returns its stats as mentioned below
1. Maximum number in the container
2. Minimum number in the container
3. Sum of squares of elements in the container.

## Functionality

### **Directory structure**
```
.
├── app
│   └── main.cpp
├── build
│   ├── CMakeCache.txt
│   ├── CMakeFiles
│   │   ├── 3.16.3
│   │   │   ├── CMakeCCompiler.cmake
│   │   │   ├── CMakeCXXCompiler.cmake
│   │   │   ├── CMakeDetermineCompilerABI_C.bin
│   │   │   ├── CMakeDetermineCompilerABI_CXX.bin
│   │   │   ├── CMakeSystem.cmake
│   │   │   ├── CompilerIdC
│   │   │   │   ├── a.out
│   │   │   │   ├── CMakeCCompilerId.c
│   │   │   │   └── tmp
│   │   │   └── CompilerIdCXX
│   │   │       ├── a.out
│   │   │       ├── CMakeCXXCompilerId.cpp
│   │   │       └── tmp
│   │   ├── cmake.check_cache
│   │   ├── CMakeDirectoryInformation.cmake
│   │   ├── CMakeOutput.log
│   │   ├── CMakeTmp
│   │   ├── main.dir
│   │   │   ├── app
│   │   │   │   └── main.cpp.o
│   │   │   ├── build.make
│   │   │   ├── cmake_clean.cmake
│   │   │   ├── CXX.includecache
│   │   │   ├── DependInfo.cmake
│   │   │   ├── depend.internal
│   │   │   ├── depend.make
│   │   │   ├── flags.make
│   │   │   ├── link.txt
│   │   │   ├── progress.make
│   │   │   └── src
│   │   │       └── Containers.cpp.o
│   │   ├── Makefile2
│   │   ├── Makefile.cmake
│   │   ├── progress.marks
│   │   └── TargetDirectories.txt
│   ├── cmake_install.cmake
│   ├── main
│   └── Makefile
├── CMakeLists.txt
├── include
│   └── Containers.h
├── ReadME.md
└── src
    └── Containers.cpp
```

here, **build** folder has make,Cmake related files which are used to build this project. **App** folder consists of the main.cpp file. **src** folder consists of implementation file called Containers.cpp and **include** folder consists of the header file Containers.h

### **Working**
The main program starts with initializing two containers, one for checking the functionality of a list and the other for a vector. Then we create a temporary list and a temporary vector and use set functions to calculate the stats of the containers and store them in class variables. We use get functions to get the values and print them on the screen.

### **Instructions**
1. Clone this repo to your personal computer or mac.
2. Create a build folder.
3. Run "cmake .." command from build folder.
4. Run "make" from build folder.