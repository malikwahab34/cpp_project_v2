#include <iostream>
#include <list>
#include <vector>
#include <Containers.h>
//#include <benchmark/benchmark.h>
#include "benchmark.h"
#include <cstdlib>
#include <algorithm>

using namespace std;

Containers example_vector_container;
Containers example_list_container;

static void BM_VECTOR(benchmark::State& state)
{
    for (auto _ : state)
    {
        vector<int> example_vector(state.range(0));
        example_vector.reserve(state.range(0));
        generate(example_vector.begin(), example_vector.end(), rand);
        example_vector_container.set(example_vector);
    }
}

static void BM_LIST(benchmark::State& state)
{
    for(auto _ : state)
    {
        list<int> example_list(state.range(0));
        generate(example_list.begin(), example_list.end(), rand);
        example_list_container.set(example_list);
    }
}

static void BM_VECTOR_Million(benchmark::State& state)
{
     for (auto _ : state)
    {
        vector<int> example_vector(4000000);
        generate(example_vector.begin(), example_vector.end(), rand);
        example_vector_container.set(example_vector);
    }
}

BENCHMARK(BM_VECTOR) -> Arg(10000) -> Arg(20000) -> Arg(30000);
BENCHMARK(BM_LIST) -> Arg(10000) -> Arg(20000) -> Arg(30000);
BENCHMARK(BM_VECTOR_Million);

BENCHMARK_MAIN();